import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {BEARER_SECURITY_SPEC} from '../utils/security-spec';
import {Supplier} from '../models';
import {SupplierRepository} from '../repositories';

@authenticate('jwt')
@authorize.scope('supplier')
export class SupplierController {
  constructor(
    @repository(SupplierRepository)
    public supplierRepository: SupplierRepository,
  ) {}
  @post('/suppliers', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Supplier model instance',
        content: {'application/json': {schema: getModelSchemaRef(Supplier)}},
      },
    },
  })
  // @authenticate('jwt')
  @authorize({scopes: ['create']})
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Supplier, {
            title: 'NewSupplier',
            exclude: ['id'],
          }),
        },
      },
    })
    supplier: Omit<Supplier, 'id'>,
  ): Promise<Supplier> {
    return this.supplierRepository.create(supplier);
  }

  @get('/suppliers/count', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Supplier model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Supplier))
    where?: Where<Supplier>,
  ): Promise<Count> {
    return this.supplierRepository.count(where);
  }

  @get('/suppliers', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Supplier model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Supplier, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Supplier))
    filter?: Filter<Supplier>,
  ): Promise<Supplier[]> {
    return this.supplierRepository.find(filter);
  }

  @get('/suppliers/{id}', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Supplier model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Supplier, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Supplier))
    filter?: Filter<Supplier>,
  ): Promise<Supplier> {
    return this.supplierRepository.findById(id, filter);
  }

  @put('/suppliers/{id}', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Supplier PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() supplier: Supplier,
  ): Promise<void> {
    await this.supplierRepository.replaceById(id, supplier);
  }

  @del('/suppliers/{id}', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Supplier DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.supplierRepository.deleteById(id);
  }
}
