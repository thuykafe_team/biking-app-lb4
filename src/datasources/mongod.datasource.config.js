export default {
  name: 'mongod',
  connector: 'mongodb',
  url: '',
  hostname: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 27017,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: 'bikingapp-db',
  useNewUrlParser: true,
};
