import {BikingAppApplication} from './application';
import {HelloApplication} from './hello.application';
import {ApplicationConfig} from '@loopback/core';

export {BikingAppApplication, HelloApplication};

export async function main(
  options: ApplicationConfig = {},
  public_options: ApplicationConfig = {},
) {
  const app = new BikingAppApplication(options);
  const hello = new HelloApplication(public_options);

  await app.boot();
  await app.start();

  await hello.boot();
  await hello.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);

  const helloUrl = hello.restServer.url;
  console.log(`Server is running at ${helloUrl}`);

  return app;
}
