import {SecuritySchemeObject, ReferenceObject} from '@loopback/openapi-v3';

export type SecuritySchemeObjects = {
  [securityScheme: string]: SecuritySchemeObject | ReferenceObject;
};

export const BASIC_SECURITY_SPEC = [{basic: ['basic']}];
export const BASIC_SECURITY_SCHEMA_SPEC: SecuritySchemeObjects = {
  basic: {
    type: 'http',
    scheme: 'basic',
    in: 'header',
  },
};

export const BEARER_SECURITY_SPEC = [{bearerAuth: []}];
export const BEARER_SECURITY_SCHEMA_SPEC: SecuritySchemeObjects = {
  bearerAuth: {
    type: 'http',
    scheme: 'bearer',
    bearerFormat: 'JWT',
  },
};
