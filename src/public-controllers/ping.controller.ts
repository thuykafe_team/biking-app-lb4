import {inject} from '@loopback/context';
import {get, post, param, requestBody, api} from '@loopback/rest';
import {authenticate} from '@loopback/authentication';
import {BEARER_SECURITY_SPEC} from '../utils/security-spec';
import {SecurityBindings, UserProfile} from '@loopback/security';

export class PingController {
  constructor() {}

  @get('/auth', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {content: 'application/json'},
    },
  })
  @authenticate('jwt')
  async getHello(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<string> {
    const result = `Hello user ${currentUserProfile.name} has email ${currentUserProfile.email}`;
    return result;
  }

  @get('/hi', {
    responses: {
      '200': {
        content: 'application/json',
      },
    },
  })
  hello(@param.query.string('name') name: string) {
    return `Hello ${name}`;
  }

  @post('/foo')
  postHello(
    @param.query.string('pwResetToken') pwResetToken: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email', 'password'],
            properties: {
              email: {
                type: 'string',
                format: 'email',
              },
              // eslint-disable-next-line @typescript-eslint/camelcase
              new_password: {
                type: 'string',
                minLength: 8,
              },
            },
          },
        },
      },
    })
    cred: {
      email: string;
      password: string;
    },
  ): string {
    return 'Hello ' + cred.email + ' with token ' + pwResetToken;
  }
}
