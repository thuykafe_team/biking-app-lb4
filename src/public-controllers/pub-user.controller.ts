// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/context';
import {
  get,
  post,
  param,
  requestBody,
  getModelSchemaRef,
  HttpErrors,
} from '@loopback/rest';
import {SecurityBindings, UserProfile, securityId} from '@loopback/security';
import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {randomBytes} from 'crypto';
import {UserRepository} from '../repositories';
import {User, UserCredentials, NewUserRequest} from '../models';
import {Credentials} from '../repositories/user.repository';
import {BEARER_SECURITY_SPEC} from '../utils/security-spec';
import {
  AuthenticationUserService,
  JWTokenService,
  PasswordHasher,
  MailService,
  hashPassword,
} from '../services';
import {
  UserServiceBindings,
  TokenServiceBindings,
  PasswordHasherBindings,
  MailServiceBindings,
} from '../keys';

export class PublicUserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: AuthenticationUserService,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public tokenService: JWTokenService,
    public invalidCredentialsError = 'Invalid email or password.',
    public invalidResetTokenError = 'Invalid reset token',
  ) {}

  @post('/users/signup', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async create(
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    passwordHasher: PasswordHasher,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<{token: string}> {
    if (!newUserRequest.password) {
      throw new HttpErrors.BadRequest('Password must not be null');
    }
    // encrypt the password
    const password = await passwordHasher.hashPassword(newUserRequest.password);
    try {
      const {password, ...rest} = newUserRequest;
      const user = {...rest};
      const savedUser = await this.userRepository.create(user);

      // set the password
      await this.userRepository
        .userCredentials(savedUser.id)
        .create({password});

      const userProfile = this.userService.convertToUserProfile(savedUser);

      const token = await this.tokenService.generateToken(userProfile);
      return {token};
    } catch (error) {
      // MongoError 11000 duplicate key
      if (
        error.code === 11000 ||
        error.errmsg.includes('index: email_1 dup key')
      ) {
        const duplicateEmail = error.keyValue.email;
        throw new HttpErrors.Conflict(
          `Email ${duplicateEmail} is already taken`,
        );
      } else {
        throw error;
      }
    }
  }

  @get('/users/reset-password', {
    responses: {
      '204': {
        description: 'Reset password success, please check your inbox',
      },
    },
  })
  async resetPasswordRequest(
    @inject(MailServiceBindings.MAIL_SERVICE) mailService: MailService,
    @param.query.string('email') email: string,
  ): Promise<void> {
    const resetToken = randomBytes(16).toString('hex');

    // Find the desired user
    const foundUser = await this.userRepository.findOne({
      where: {email},
    });
    if (!foundUser)
      throw new HttpErrors.Unauthorized(this.invalidCredentialsError);

    try {
      await mailService.sendEmailResetPassword(resetToken, {
        name: `${foundUser.firstName} ${foundUser.lastName}`,
        address: email,
      });

      // update password for want-to-change user
      const hashedToken = await hashPassword(resetToken, 10);
      const newCredentials: Partial<UserCredentials> = {
        password: hashedToken,
      };
      await this.userRepository
        .userCredentials(foundUser.id)
        .patch(newCredentials);
      return;
    } catch (error) {
      throw new Error(error);
    }
  }

  @post('/users/reset-password-request', {
    responses: {
      '200': {
        description: 'Successfully reset password',
        content: {'application/json': {schema: User}},
      },
    },
  })
  async resetPassword(
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    passwordHasher: PasswordHasher,
    @param.query.string('pwResetToken') pwResetToken: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email', 'password'],
            properties: {
              email: {
                type: 'string',
                format: 'email',
              },
              // eslint-disable-next-line @typescript-eslint/camelcase
              new_password: {
                type: 'string',
                minLength: 8,
              },
            },
          },
        },
      },
    })
    credentials: Credentials,
  ): Promise<User> {
    if (pwResetToken === '') throw new HttpErrors.BadRequest('Bad Request');

    const foundUser = await this.userRepository.findOne({
      where: {email: credentials.email},
    });
    if (!foundUser) throw new HttpErrors.NotFound('User not found');
    const exstingCredentials = await this.userRepository
      .userCredentials(foundUser.id)
      .get();

    const isCorrectResetToken = await passwordHasher.comparePassword(
      pwResetToken,
      exstingCredentials.password,
    );

    if (!isCorrectResetToken)
      throw new HttpErrors.Unauthorized(this.invalidResetTokenError);

    const newPassword = await passwordHasher.hashPassword(credentials.password);
    const newCred: Partial<UserCredentials> = {password: newPassword};

    await this.userRepository.userCredentials(foundUser.id).patch(newCred);
    return foundUser;
  }

  @get('/users/me', {
    security: BEARER_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<User> {
    currentUserProfile.id = currentUserProfile[securityId];
    delete currentUserProfile[securityId];
    const id = currentUserProfile.id;
    return this.userRepository.findById(id);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token issued for User',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody({
      description: 'The input of login function',
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email', 'password'],
            properties: {
              email: {
                type: 'string',
                format: 'email',
              },
              password: {
                type: 'string',
                minLength: 8,
              },
            },
          },
        },
      },
    })
    credentials: Credentials,
  ): Promise<{token: string}> {
    //ensure entity is exsit with corrent credential info
    const user = await this.userService.verifyCredentials(credentials);

    // convert user to userProfile, as in SecurityBindings
    const userProfile = this.userService.convertToUserProfile(user);

    const token = await this.tokenService.generateToken(userProfile);
    return {token};
  }
}
