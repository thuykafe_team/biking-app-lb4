import {model, property, Model} from '@loopback/repository';
@model()
export class NewUserRequest extends Model {
  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  email: string;

  @property({
    type: 'string',
  })
  firstName?: string;

  @property({
    type: 'string',
  })
  lastName?: string;

  @property({
    type: 'string',
  })
  avatar?: string;

  @property({
    type: 'number',
  })
  age?: number;

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['male', 'female', 'other'],
    },
    default: () => 'other',
  })
  gender?: string;

  constructor(data?: Partial<NewUserRequest>) {
    super(data);
  }
}
export interface NewUserRequestRelations {
  // describe navigational properties here
}

export type NewUserRequestWithRelations = NewUserRequest &
  NewUserRequestRelations;
