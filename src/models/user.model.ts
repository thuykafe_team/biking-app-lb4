import {Entity, model, property, hasOne} from '@loopback/repository';
import {
  UserCredentials,
  UserCredentialsWithRelations,
} from './user-credentials.model';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    mongodb: {dataType: 'ObjectID'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  email: string;

  @property({
    type: 'string',
  })
  firstName?: string;

  @property({
    type: 'string',
  })
  lastName?: string;

  @property({
    type: 'string',
  })
  avatar?: string;

  @property({
    type: 'number',
  })
  age?: number;

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['male', 'female', 'other'],
    },
    default: () => 'other',
  })
  gender?: string;

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  // Other properties if need to initilize
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  userCredentials?: UserCredentialsWithRelations;
}

export type UserWithRelations = User & UserRelations;
