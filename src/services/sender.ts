import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';

export const createTestAccount = () => nodemailer.createTestAccount();
export const createTransporter = (account: nodemailer.TestAccount) =>
  nodemailer.createTransport(
    {
      host: account.smtp.host,
      port: account.smtp.port,
      secure: account.smtp.secure,
      auth: {
        user: account.user,
        pass: account.pass,
      },
      logger: true,
      debug: false, // include SMTP traffic in the logs
    },
    {
      // default message fields

      // sender info
      from: 'Me <tester.abcxyz.12345@gmail.com>',
    },
  );

export async function createMailTransport(): Promise<nodemailer.Transporter> {
  /**
   * Use to assert operation x! produce value non-null and non-undefined
   * See more at: https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-7.html#definite-assignment-assertions
   * */
  let transporter!: nodemailer.Transporter;
  let testAccount!: nodemailer.TestAccount;
  try {
    testAccount = await createTestAccount();
    transporter = createTransporter(testAccount);
  } catch (error) {
    console.log(error);
  }
  return transporter;
}

export async function createGmailTransport() {
  return nodemailer.createTransport(
    smtpTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      auth: {
        user: 'tester.abcxyz.123456@gmail.com',
        pass: 'DemonstrateGmail!',
      },
    }),
  );
}
