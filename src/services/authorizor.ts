import {bind, inject, BindingScope, Provider} from '@loopback/core';
import {
  Authorizer,
  AuthorizationContext,
  AuthorizationMetadata,
  AuthorizationRequest,
  AuthorizationDecision,
} from '@loopback/authorization';
import * as casbin from 'casbin';
import {AuthorizationBindings} from '../keys';

export type CasbinEnforcer = casbin.Enforcer;
@bind({scope: BindingScope.TRANSIENT})
export class CasbinAuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @inject(AuthorizationBindings.CASBIN_ENFORCER)
    private enforcer: CasbinEnforcer,
  ) {}

  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    context: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    /**
     * form a request, sending to Enforcer, from context and metadata
     * @param sub := context.currentUserProfile.name
     * @param obj := metadata.resource OR context.resource (default: AUTHORIZATION_CLASS_KEY for class, AUTHORIZATION_METHOD_KEY for method)
     * @param act := metadata.scopes if scopes.length > 0 OR 'some-default-action'
     * */

    const request: AuthorizationRequest = {
      subject: context.principals[0].name,
      object: metadata.resource ?? context.resource,
      action: (metadata.scopes && metadata.scopes[0]) || 'execute',
    };

    const allow = await this.enforcer.enforce(
      request.subject,
      request.object,
      request.action,
    );

    if (allow) return AuthorizationDecision.ALLOW;
    else if (!allow) return AuthorizationDecision.DENY;
    return AuthorizationDecision.ABSTAIN;
  }
}
