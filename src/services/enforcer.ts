import * as casbin from 'casbin';
import path from 'path';

export async function createEnforcer() {
  /**
   * Those files is extracted from offical website of casbin project
   * see more at: https://casbin.org/docs/en/supported-models
   */
  const conf = path.resolve(__dirname, '../../fixtures/casbin/rbac_model.conf');
  const policy = path.resolve(
    __dirname,
    '../../fixtures/casbin/rbac_policy.csv',
  );
  return casbin.newEnforcer(conf, policy);
}
