import {bind, BindingScope} from '@loopback/core';
import {inject} from '@loopback/context';
import {URLSearchParams} from 'url';
import * as OriginNodeMailer from 'nodemailer';
import {MailServiceBindings} from '../keys';
import {createTestAccount, createTransporter} from './sender';

export function composeLetter(
  url: string,
  recipientEmail: string,
): OriginNodeMailer.SendMailOptions {
  const letter = {
    from: MyMail.SENDER,
    to: recipientEmail,
    subject: 'Sending mail from NodeMailer_BikingApp',
    text: `Dear user,\n\nYou can reset your password by going to ${url}`,
    html: `
                <p>Dear user,</p>
    <p>
                    You can reset your password by going to
                    <a href="${url}">this link</a>
                </p>
            `,
  };
  return letter;
}

namespace MyMail {
  export const SENDER = 'tester.abcxyz.123456@gmail.com';
  export interface Address {
    name?: string;
    address: string;
  }

  // We use OAuth2.0 protocol to authorize request
  type AuthSubjectType = 'OAuth2';
  export interface AuthSubject {
    type: AuthSubjectType;
    user: string;
    clientId: string; // the registered client id of the application in GApps
    clientSecret: string; // the registered client secret of the application in GApps
    refreshToken: string; // use to generate a new access token if existing one expires or fails
    accessToken: string; // access token for the user. Required only if refreshToken is not available and there is no token refresh callback specified
    expries?: number;
    accessUrl?: string; // HTTP endpoint for requesting new access tokens, defaults to Gmail
  }
  export interface LessSecureAuthSubject {
    user: string;
    pass: string;
  }
}

export type AuthSubject = MyMail.AuthSubject | MyMail.LessSecureAuthSubject;

export type Transporter = OriginNodeMailer.Transporter;

export interface MailService {
  build: () => Promise<void>;
  verify: () => Promise<true>;
  close: () => void;
  sendMail: (
    args: OriginNodeMailer.SendMailOptions,
  ) => Promise<OriginNodeMailer.SentMessageInfo>;
  sendEmailResetPassword: (
    rsToken: string,
    recipient: OriginNodeMailer.SendMailOptions | MyMail.Address,
  ) => Promise<OriginNodeMailer.SentMessageInfo | Error | boolean>;
}

@bind({scope: BindingScope.TRANSIENT})
export class NodeMailer implements MailService {
  testAccount: OriginNodeMailer.TestAccount;
  constructor(
    @inject(MailServiceBindings.TRANSPORTER)
    protected _transporter: OriginNodeMailer.Transporter,
  ) {}

  async build(): Promise<void> {
    try {
      this.testAccount = await createTestAccount();
    } catch (error) {
      console.error(error);
    }
    this._transporter = createTransporter(this.testAccount);
  }

  foo() {
    console.log(this._transporter);
    console.log('foo');
  }

  verify() {
    return this._transporter.verify();
  }

  close() {
    return this._transporter.close();
  }

  sendMail(
    letter: OriginNodeMailer.SendMailOptions,
  ): Promise<OriginNodeMailer.SentMessageInfo> {
    return this._transporter.sendMail(letter);
  }

  async sendEmailResetPassword(
    rsToken: string, // acts like concurrent user's password
    recipient: OriginNodeMailer.SendMailOptions | MyMail.Address,
  ): Promise<OriginNodeMailer.SentMessageInfo | Error | boolean> {
    const isAddress = (
      x: OriginNodeMailer.SendMailOptions | MyMail.Address,
    ): x is MyMail.Address => {
      return (x as MyMail.Address).address !== undefined;
      // return x;
    };
    if (isAddress(recipient)) {
      const params = new URLSearchParams({
        pwResetToken: rsToken,
        email: recipient.address,
      });
      const resetPasswordUrl = `${
        process.env.FRONTEND_URL
      }/resetPassword?${params.toString()}`;
      recipient = composeLetter(resetPasswordUrl, recipient.address);
    }
    return this.sendMail(recipient)
      .then(info => {
        console.log(`Sent mail successed,${info}}`);
        this.close();
        return OriginNodeMailer.getTestMessageUrl(info);
      })
      .catch(err => {
        this.close();
        throw new Error(err);
      });
  }
}
