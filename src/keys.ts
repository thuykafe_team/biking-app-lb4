import {BindingKey} from '@loopback/context';
import {Authorizer} from '@loopback/authorization';
import {
  AuthenticationUserService,
  PasswordHasher,
  JWTokenService,
  CasbinEnforcer,
  MailService,
  Transporter,
} from './services';
import {BasicAuthOptions, JWTOptions} from './authentication-strategies';
import {PackageInfo} from './application';

export namespace ApplicationBindings {
  export const PackageKey = BindingKey.create<PackageInfo>(
    'application.package',
  );
}
export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<AuthenticationUserService>(
    'services.authentication.user.service',
  );
}
export namespace AuthenticationStrategyBindings {
  export namespace Basic {
    export const DEFAULT_OPTIONS = BindingKey.create<BasicAuthOptions>(
      'authentication.strategy.basic.defaultoptions',
    );
  }
  export namespace JWT {
    export const DEFAULT_OPTIONS = BindingKey.create<JWTOptions>(
      'authentication.strategy.jwt.defaultoptions',
    );
  }
}
export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'services.hasher',
  );

  export const ROUNDS = BindingKey.create<number>('services.hasher.round');
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>(
    'authentication.jwt.expires.in.seconds',
  );
  export const TOKEN_SERVICE = BindingKey.create<JWTokenService>(
    'services.authentication.jwt.tokenservice',
  );
}

export namespace AuthorizationBindings {
  export const CASBIN_ENFORCER = BindingKey.create<CasbinEnforcer>(
    'casbin.enforcer',
  );

  export const CASBIN_AUTHORIZATION_PROVIDER = BindingKey.create<Authorizer>(
    'authorizationProviders.casbin-provider',
  );
}
export namespace MailServiceBindings {
  export const MAIL_SERVICE = BindingKey.create<MailService>('services.mailer');

  export const TRANSPORTER = BindingKey.create<Transporter>(
    'services.mailer.transporter',
  );
}
