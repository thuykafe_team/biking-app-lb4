import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import {
  AuthenticationComponent,
  registerAuthenticationStrategy,
} from '@loopback/authentication';
import {
  AuthorizationComponent,
  AuthorizationTags,
} from '@loopback/authorization';
import * as path from 'path';
import * as dotenv from 'dotenv';
import {MySequence} from './sequence';
import {
  ApplicationBindings,
  AuthenticationStrategyBindings,
  PasswordHasherBindings,
  TokenServiceBindings,
  UserServiceBindings,
  AuthorizationBindings,
  MailServiceBindings,
} from './keys';
import {
  AuthenticationUserService,
  BcryptHasher,
  JWTokenService,
  NodeMailer,
  createGmailTransport,
  CasbinAuthorizationProvider,
  createEnforcer,
} from './services';
import {BEARER_SECURITY_SCHEMA_SPEC} from './utils/security-spec';
import {
  BasicAuthenticationStrategy,
  JWTAuthenticationStategy,
} from './authentication-strategies';
/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string;
  version: string;
  description: string;
}

const pkg: PackageInfo = require('../package.json');

export class BikingAppApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up dotenv
    dotenv.config({path: path.join(__dirname, '../.env')});

    this.api({
      openapi: '3.0.0',
      info: {
        title: pkg.name,
        version: pkg.version,
        description: 'This is internal server',
      },
      paths: {},
      components: {securitySchemes: BEARER_SECURITY_SCHEMA_SPEC},
    });

    // Bind all declared custom made keys in '../keys' to correct value
    this.setupBindings();

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));
    this.static(
      '/resetPassword',
      path.join(__dirname, '../public/resetPassword'),
    );

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    // Add authentication component
    this.component(AuthenticationComponent);
    this.component(AuthorizationComponent);

    // and register our stategy to this very app
    registerAuthenticationStrategy(this, BasicAuthenticationStrategy);
    registerAuthenticationStrategy(this, JWTAuthenticationStategy);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  setupBindings(): void {
    // Bind package.json to the application context
    this.bind(ApplicationBindings.PackageKey).to(pkg);

    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(PasswordHasherBindings.ROUNDS).to(10);

    this.bind(UserServiceBindings.USER_SERVICE).toClass(
      AuthenticationUserService,
    );

    // authentication
    this.bind(AuthenticationStrategyBindings.Basic.DEFAULT_OPTIONS).to({});
    this.bind(AuthenticationStrategyBindings.JWT.DEFAULT_OPTIONS).to({});

    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTokenService);
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      process.env.JWT_SECRET ?? 'eggs',
    );
    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      process.env.JWT_EXPIRES_IN ?? '3h',
    );

    // authorization
    this.bind(AuthorizationBindings.CASBIN_ENFORCER).toDynamicValue(
      createEnforcer,
    );
    this.bind(AuthorizationBindings.CASBIN_AUTHORIZATION_PROVIDER)
      .toProvider(CasbinAuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER);

    // mailService
    this.bind(MailServiceBindings.MAIL_SERVICE).toClass(NodeMailer);
    this.bind(MailServiceBindings.TRANSPORTER).toDynamicValue(
      createGmailTransport,
    );
  }
}
