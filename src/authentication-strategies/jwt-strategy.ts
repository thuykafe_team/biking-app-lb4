import {Request, HttpErrors} from '@loopback/rest';
import {AuthenticationStrategy} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {UserProfile} from '@loopback/security';
import {TokenServiceBindings, AuthenticationStrategyBindings} from '../keys';
import {JWTokenService} from '../services';

export interface JWTOptions {
  [property: string]: any;
}

export class JWTAuthenticationStategy implements AuthenticationStrategy {
  name = 'jwt';

  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: JWTokenService,
    @inject(AuthenticationStrategyBindings.JWT.DEFAULT_OPTIONS)
    public readonly options?: JWTOptions,
  ) {}

  async authenticate(
    request: Request,
    options?: JWTOptions,
  ): Promise<UserProfile> {
    options = options ?? this.options;
    let userProfile: UserProfile;
    try {
      const token = this.extractToken(request);
      userProfile = await this.jwtService.verifyToken(token);
    } catch (error) {
      throw error;
    }
    return userProfile;
  }

  extractToken(request: Request): string {
    if (!request.headers.authorization) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }
    const authHeaderValue = request.headers.authorization;

    if (!authHeaderValue.startsWith('Bearer')) {
      throw new HttpErrors.Unauthorized(
        `Authorization header is not of type 'Bearer'.`,
      );
    }

    const parts = authHeaderValue.split(' ');
    if (parts.length !== 2)
      throw new HttpErrors.BadRequest(
        `Authorization header value has too many parts. It must follow the pattern: 'Bearer token'`,
      );
    const token = parts[1];
    const tokenParts = token.split('.');

    if (tokenParts.length !== 3)
      throw new HttpErrors.BadRequest(
        `Token is invalid, must follow the pattern: xx.yy.zz`,
      );
    return token;
  }
}
