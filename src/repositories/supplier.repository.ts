import {DefaultCrudRepository} from '@loopback/repository';
import {Supplier, SupplierRelations} from '../models';
import {MongodDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class SupplierRepository extends DefaultCrudRepository<
  Supplier,
  typeof Supplier.prototype.id,
  SupplierRelations
> {
  constructor(@inject('datasources.mongod') dataSource: MongodDataSource) {
    super(Supplier, dataSource);
  }
}
