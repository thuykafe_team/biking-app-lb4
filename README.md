# lb4-biking-app

- [lb4-biking-app](#lb4-biking-app)
  - [Prologue](#prologue)
  - [Run with docker](#run-with-docker)
    - [Requirements](#requirements)
    - [Steps](#steps)
    - [**Notes**](#notes)
  - [Run with node.js](#run-with-nodejs)
    - [Requirements](#requirements-1)
    - [Steps](#steps-1)
  - [Continue Steps](#continue-steps)
  - [Run app](#run-app)

## Prologue

Use [Loopback4](http://loopback.io/) to build api explorer for [RN app biking-app]()

## Run with docker

### Requirements

- Install docker, docker-compose

### Steps

- At project app folder `<your-relativate-path>/biking-app`
- Run command
  `docker-compose -f "docker-compose.yml" up --detach --build <services>`<br/>

- Attach app container to shell<br/>
  `docker exec -it <CONTAINER-ID> bash`
- Your terminal will look like this<br/>
  `node@<CONTAINER-ID>:~/app $`
- [Continue steps below](#continue-steps)

### **Notes**

- Defaultly, parameter `services` is omit means all services will be composed, otherwise can be which specified **<service>** be composed
- Running `docker-compose -f "docker-compose.debug.yml" up --no-build <services>` can be falsy, caused by **mongodb container** must be up-and-running first and foremost. Make sure **db** container is running well before run **app** container

## Run with node.js

### Requirements

- Install Node >= 8 LTS, npm, yarn (optional), nodemon (optional)
- Must have mongodb, via docker or installation

### Steps

- Check node is installed

  - `node --version` must >= 8 LTS
  - Check mongodb can be connected at **localhost:27017**

- At project app folder `<your-relativate-path>/biking-app`
- Install packages `npm install`
- Start app `npm start`
- [Continue steps below](#continue-steps)

## Continue Steps

- First-time run only `npm run mirgate` to migrate database in app with datasources
- Check mongodb have database **"biking-app"**

## Run app

- Go to `http://[::1]:3000` for internal api explorer
  - `http://[::1]:5000` for public api explorer

[![LoopBack](<https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png>)](http://loopback.io/)
