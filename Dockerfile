FROM node:10-slim

# package @types/ramda depence on git ls-remote -h -t ssh://abc.git
RUN apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y git

# install nodemon in npm -g
RUN npm --global config set user root && \
  npm --global install nodemon

# Set to a non-root built-in user `node`
# Create app directory (with user `node`)
USER node
RUN mkdir -p /home/node/app

WORKDIR /home/node/app

# Install app dependencies
COPY --chown=node package*.json ./

RUN npm install
ENV PORT 3000
ENV PUBLIC_PORT 5000

# Bundle app source code
COPY --chown=node . .
CMD [ "npm", "start" ]
